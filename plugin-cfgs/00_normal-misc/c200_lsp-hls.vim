let s:present = isdirectory(expand("$NVIM_PKGS_MINPAC") . "/start/nvim-lspconfig")

if(s:present)
lua <<EOF
require'lspconfig'.hls.setup{ on_attach = custom_lsp_attach }
EOF
endif
