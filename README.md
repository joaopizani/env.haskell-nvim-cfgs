Reasonable compilation of Haskell NeoVim configs
================================================

What is this?
-------------

This repo is a compilation of the "best of the best" pieces of NeoVim configuration that I have
collected during the last one or two years of Haskell programming using NeoVim as a development
environment. My choices regarding what to include here and HOW to include it are based on these
guidelines:

  * **Cost/benefit ratio:** The functionalities provided here should be EASY to use, without
    incurring in a lot of OS hacking, and provide reasonable comfort while developing.

  * **Independence:** As much as possible, features inside this "compilation" should work for any
    distribution of NeoVim, and be helpful to as many Haskell programmers as possible, using the
    most diverse Vim environments and operating systems. The functionalities are divided in two
    main categories: plugin-independent and plugin-dependent.

  * **Graceful degradation:** This means that _as many_ features _as possible_ are enabled, but
    very few requirements are made on your NeoVim environment or OS. If you don't have what it takes
    to use something, it will _just not be available, without annoying error messages_.


What exactly is included?
-------------------------

