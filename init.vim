let $NVIM_MOD = fnamemodify(resolve(expand("<sfile>")), ":p:h")

for f in glob(expand("$NVIM_MOD") . '/simple-cfgs/**/*.vim', 1, 1) | exe 'source' f | endfor
for f in glob(expand("$NVIM_MOD") . '/plugin-cfgs/**/*.vim', 1, 1) | exe 'source' f | endfor

